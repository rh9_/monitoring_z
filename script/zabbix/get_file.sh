#!/bin/bash

#########################
##### Borisov M.N. ######
### mic_bor@rambler.ru ##
#########################

umask 077
source /usr/script/zabbix/config.cfg

cmd_userid="$1"
hostid="$2"
dest_input_path="$3"
dest_file="$4"

if [ "${dest_input_path: -1}" == "/" ];then
        dest_path="$dest_input_path"
else
        dest_path="$dest_input_path/"
fi

PSQL_ss="sudo -u postgres psql -X -A -h $IP_ZABBIX_PGSQL -U $LOGIN_ZABBIX_PGSQL -d $DB_ZABBIX_PGSQL -t -c"

if [ $# -lt 3 ];then
        echo "use: userid host file_get [groupid]"
        exit 1
fi

if [ ! -e /tmp/ram/cmd_z ];then
        mkdir -m 0600 /tmp/ram/cmd_z
fi

if [ ! -e /tmp/cmd_z ];then
        mkdir -m 0600 /tmp/cmd_z
fi

#$query  = "select host from hosts where hostid=$host_list";
dest_hostname=$($PSQL_ss "SELECT host FROM hosts WHERE hostid=$hostid")

function get_from_key_file ()
{
	security_key_l=$1
        cat /usr/script/zabbix/key.p | grep $security_key_l | cut -f2 -d: | while read -n1 C ; do if [ ! -z $C ]; then sed -n "$(sed -n "/$C/=" /usr/script/zabbix/keyout)p" /usr/script/zabbix/keyin | tr -d \\n; fi ; done ; echo
}

mlogin=$(get_from_key_file $kroot $sfile_login)
mpassword=$(get_from_key_file $krootpassword)


#hostnames=$($PSQL_ss "SELECT host FROM host_in_group WHERE groupid=$groupid")

# временный файл в /tmp/cmd_z/
source_file=$RANDOM$RANDOM$RANDOM$RANDOM$RANDOM
# права на файл и другие атрибуты
source_file_context=$RANDOM$RANDOM$RANDOM$RANDOM$RANDOM

export mlogin="$mlogin"
export mpassword="$mpassword"
/usr/script/zabbix/get_file.expect $dest_hostname $dest_path $dest_file $source_file $source_file_context > /dev/null 2>&1

echo "$source_file $source_file_context"

